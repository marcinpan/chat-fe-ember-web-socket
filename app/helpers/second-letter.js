import { helper } from '@ember/component/helper';

export function secondLetter(params/*, hash*/) {
  return params[0][1];
}

export default helper(secondLetter);
