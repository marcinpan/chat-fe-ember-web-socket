import DS from 'ember-data';

export default DS.Model.extend({
  author: DS.attr('string'),
  recipient: DS.attr('string'),
  body: DS.attr('string'),
  createdAt: DS.attr('date', {
    defaultValue() { return new Date(); }
  }),
  updatedAt: DS.attr('date', {
    defaultValue() { return new Date(); }
  })
});
