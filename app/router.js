import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('login');
  this.route('signup');
  this.route('chat', function() {
    this.route('user', { path: '/:user_id' });
  });
  this.route('badurl', { path: '/*badurl' });
});

export default Router;
