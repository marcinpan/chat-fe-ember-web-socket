import Service from '@ember/service';
import { get } from "@ember/object";
import { assign } from "@ember/polyfills"

export default Service.extend({
  init() {
    if(localStorage.getItem('user')) {
      assign(this, JSON.parse(localStorage.getItem('user')));
    }
  },
  storeUser(userModel) {
    const user = {
      id: get(userModel, 'id'),
      username: get(userModel, 'username'),
      email: get(userModel, 'email')
    };
    assign(this, user);
    localStorage.setItem('user', JSON.stringify(user));
  },
  removeUser() {
    localStorage.removeItem('user');
    delete this.id;
    delete this.username;
    delete this.email;
  }
});
