import Service from '@ember/service';
import { get, set } from "@ember/object";
import { inject as service } from '@ember/service';

export default Service.extend({
  socketIo: service(),
  socketUrl: 'http://localhost:3100/',
  init() {
    this._super(...arguments);
    set(this, 'socket', this.get('socketIo').socketFor(get(this, 'socketUrl')));
  }
});
