import Route from '@ember/routing/route';
import { get } from "@ember/object";
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    return get(this, 'store').findAll('message');
  }
});
