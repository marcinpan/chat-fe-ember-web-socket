import Route from '@ember/routing/route';
import { get } from "@ember/object";
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  currentUser: service(),
  store: service(),
  model() {
    return get(this, 'store').findAll('user').then(users => users.filter(user => {
      const loggedUser = (get(user, 'email') === get(this, 'currentUser.email'));
      if (loggedUser && !get(this, 'currentUser.id')) {
        get(this, 'currentUser').storeUser(user);
      }
      return !loggedUser;
    }));
  }
});
