import Controller from '@ember/controller';
import { get, set, computed } from "@ember/object";
import { inject as service } from '@ember/service';

export default Controller.extend({
  webSocket: service(),
  currentUser: service(),
  store: service(),
  messages: computed('model', function() {
    return get(this, 'model').toArray().map( message => {
      message.set('authorUsername', get(this, 'store').peekRecord('user', message.get('author')).get('username'));
      return message;
    });
  }),
  init() {
    this._super(...arguments);
    set(this,'socket', get(this, 'webSocket.socket'));
    get(this,'socket').on(get(this, 'currentUser.id'), get(this, 'messageHandler'), this)
  },
  messageHandler(data) {
    const message = {
      author: data.author,
      authorUsername: get(this, 'store').peekRecord('user', data.author).get('username'),
      body: data.body,
      recipient: data.recipient,
      createdAt: new Date(data.createdAt),
      updatedAt: new Date(data.updatedAt)
    };
    const messageRecord = get(this, 'store').createRecord('message', message);
    get(this, 'messages').addObject(messageRecord);
  },
  actions: {
    sendMessage() {
      const message = {
        author: get(this, 'currentUser.id'),
        body: get(this, 'value'),
        recipient: get(this, 'recipient.id'),
        createdAt: new Date(),
        updatedAt: new Date()
      };
      get(this, 'socket').emit('private', message);
      set(message, 'authorUsername', get(this, 'currentUser.username'));
      const messageRecord = get(this, 'store').createRecord('message', message);
      get(this, 'messages').addObject(messageRecord);
      set(this, 'value', '');
    }
  }
});
