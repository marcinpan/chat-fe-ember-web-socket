import Route from '@ember/routing/route';
import { get, set } from "@ember/object";
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  currentUser: service(),
  model(params) {
    set(this, 'recipient', get(this, 'store').peekRecord('user', params.user_id));
    return get(this, 'store').query('message', {
      recipientId: params.user_id,
      authorId:  get(this, 'currentUser.id')
    })
  },
  setupController(controller, model) {
    this._super(controller, model);
    controller.set('recipient', this.get('recipient'));
  }
});
