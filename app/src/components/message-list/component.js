import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  store: service(),
  classNames: ['messages'],
  didRender() {
    this._super(...arguments);
    let element = document.querySelector(".main__container");
    if(element) {
      // element.scrollTop = element.scrollHeight;
      element.scrollTo(0,element.scrollHeight);
    }
  }
});
