import Component from '@ember/component';
import { get } from "@ember/object";
import { inject as service } from '@ember/service';

export default Component.extend({
  session: service(),
  currentUser: service(),
  tagName: 'div',
  classNames: ['container'],
  actions: {
    invalidateSession() {
      get(this, 'currentUser').removeUser();
      get(this, 'session').invalidate();
    }
  }
});
