import Component from '@ember/component';
import { set } from '@ember/object';

export default Component.extend({
  classNames: ['input-group', 'form-group-no-border', 'input-lg'],
  classNameBindings: ['isActive:input-group-focus'],
  tagName: 'div',
  focusIn() {
    set(this, 'isActive', true);
  },
  focusOut() {
    set(this, 'isActive', false);
  }
});
