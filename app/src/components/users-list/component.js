import Component from '@ember/component';

export default Component.extend({
  classNames: ['dm__list'],
  tagName: 'ul'
});
