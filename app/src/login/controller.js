import Controller from '@ember/controller';
import { set } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
  session: service(),
  store: service(),
  currentUser: service(),
  actions: {
    authenticate() {
      let { email, password } = this.getProperties('email', 'password');
      this.get('session')
        .authenticate('authenticator:jwt', email, password)
        .then(() => {
          set(this, 'currentUser.email', email);
          email = '';
          password = '';
        })
        .catch((message) => {
          if(message.responseJSON) {
            this.set('errorMessage', message.responseJSON.message);
          }
        });
    }
  }
});
