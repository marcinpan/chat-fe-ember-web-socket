import Controller from '@ember/controller';
import { get } from '@ember/object';
import $ from 'jquery';
import { inject as service } from '@ember/service';

export default Controller.extend({
  router: service(),
  actions: {
    signup() {
      $.post('/api/signup', {
        'username': get(this, 'login'),
        'email': get(this, 'email'),
        'password': get(this, 'password'),
      }).fail((err) => {
        this.set('errorMessage', err);
      }).done(()=> {
        this.get('router').transitionTo('login');
      });

    }
  }
});
