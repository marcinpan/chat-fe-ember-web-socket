export default function() {
  // this.transition(
  //   this.fromRoute('login'),
  //   this.toRoute('signup'),
  //   this.use('toUp'),
  //   this.reverse('toDown')
  // );
  this.transition(
    this.fromRoute('index.auth'),
    this.toRoute('index.chat'),
    this.use('toLeft'),
    this.reverse('toRight')
  );
  this.transition(
    this.fromRoute('index.chat'),
    this.toRoute('index'),
    this.use('toRight')
  );
}
