/* eslint-env node */
'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'chat-io',
    environment,
    rootURL: '/',
    locationType: 'auto',
    podModulePrefix: 'chat-io/src',
    'ember-websockets': {
      socketIO: true
    },
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    contentSecurityPolicy: {
      'default-src': "'none'",
      'script-src': "'self' 'unsafe-inline' 'unsafe-eval'",
      'font-src': "'self'",
      'connect-src': "'self' http://localhost:3000",
      'img-src': "'self'",
      'report-uri':"'localhost'",
      'style-src': "'self' 'unsafe-inline'",
      'frame-src': "'none'"
    }
  };


  ENV['ember-simple-auth'] = {
    authenticationRoute: 'login',
    store: 'simple-auth-session-store:local-storage',
    authorizer: 'authorizer:jwt',
    crossOriginWhitelist: ['http://localhost:3000/'],
    routeAfterAuthentication: 'chat',
    routeIfAlreadyAuthenticated: 'chat'
  };

  // ENV.torii = {
  //   providers: {
  //     'google-oauth2-bearer': {
  //       apiKey: "AIzaSyAfKs1TCuO6g7rspOhahOteEUXAzMys4as",
  //       redirectUri: "http://localhost:4200/oauth2callback"
  //     }
  //   }
  // };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.authServerBaseUrl = 'http://localhost:4200';
    ENV.authServerTokenEndpoint = `${ENV.authServerBaseUrl}/api/token-auth`;
    ENV.authServerRefreshTokenEndpoint = `${ENV.authServerBaseUrl}/api/token-refresh`;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV['ember-simple-auth'] = {
      store: 'simple-auth-session-store:ephemeral'
    };

    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature;
    ENV.authServerBaseUrl = 'http://localhost:4200';
    ENV.authServerTokenEndpoint = `${ENV.authServerBaseUrl}/api/token-auth`;
    ENV.authServerRefreshTokenEndpoint = `${ENV.authServerBaseUrl}/api/token-refresh`;
  }

  return ENV;
};
