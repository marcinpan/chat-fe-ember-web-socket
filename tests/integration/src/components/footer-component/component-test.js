import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('footer-component', 'Integration | Component | footer component', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(1);

  this.render(hbs`{{footer-component}}`);

  assert.equal(this.$('.footer')[0].innerText.trim(), 'MARCIN PANEK\n' +
    '2017, Designed by');
});
