import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('message-list', 'Integration | Component | message list', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(4);

  const messages = [
    {
      authorUsername: 'author1',
      createdAt: new Date('01/01/2018'),
      body: 'test message 1'
    },
    {
      authorUsername: 'author2',
      createdAt: new Date('01/01/2018'),
      body: 'test message 2'
    },
    {
      authorUsername: 'author3',
      createdAt: new Date('01/01/2018'),
      body: 'test message 3'
    }
  ];
  this.set('messages', messages);

  this.render(hbs`{{message-list messages=messages}}`);

  assert.equal(this.$('.messages').length, 1, "should display one component");
  assert.equal(this.$('.message').length, messages.length, "should display 3 messages");
  assert.deepEqual(this.$('.text').toArray().map(i => i.innerText.trim()),
    messages.map(i => i.body), 'should render messages');
  assert.deepEqual(this.$('.username').toArray().map(i => i.innerText.trim()),
    messages.map(i => i.authorUsername + ' 0:00, January 1'), 'should render usernames and date');
});
