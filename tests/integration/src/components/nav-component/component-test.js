import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('nav-component', 'Integration | Component | nav component', {
  integration: true
});

test('it renders', function(assert) {
  this.render(hbs`{{nav-component}}`);

  assert.equal(this.$('.navbar-translate').text().trim(), 'chat-io');

  this.render(hbs`
    {{#nav-component}}
      template block text
    {{/nav-component}}
  `);

  assert.equal(this.$('.navbar-translate').text().trim(), 'chat-io');
});
