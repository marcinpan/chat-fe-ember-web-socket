import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('ui-input', 'Integration | Component | ui input', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(7);

  this.render(hbs`{{ui-input value=password placeholder="Password" icon='objects_key-25' type='password'}}`);
  assert.equal(this.$('.input-group').length, 1, 'should render one input container');
  assert.equal(this.$('.input-group-addon').length, 1, 'should render one helper image');
  assert.equal(this.$('.form-control').length, 1, 'should render one input');
  assert.equal(this.$('.objects_key-25').length, 1, 'should render one icon');
  this.set('password', 'test');
  assert.equal(this.$('input').val(), 'test', 'should render property passed as value');

  assert.equal(this.$('.input-group-focus').length, 0, 'should not render focus class');
  this.$('.form-control').focusin();
  assert.equal(this.$('.input-group-focus').length, 1, 'should render focus class on focus');
});
