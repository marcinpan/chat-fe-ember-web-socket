import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('users-list', 'Integration | Component | users list', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(3);

  this.set('list', [
    {
      id: 1,
      username: 'test1'
    },
    {
      id: 2,
      username: 'test2'
    }
  ]);
  this.render(hbs`{{users-list users=list}}`);

  assert.equal(this.$('.dm__list').length, 1, 'should render one list');
  assert.equal(this.$('.dm__item').length, this.get('list').length, 'should render two list items');
  assert.deepEqual(this.$('.dm__item').toArray().map(i => i.innerText.trim()),
    this.get('list').map(i => i.username), 'should render proper names');
});
