import { moduleFor, test } from 'ember-qunit';

moduleFor('service:current-user', 'Unit | Service | current user', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  const service = this.subject();
  assert.ok(service);
  service.removeUser();
  assert.equal(service.id, undefined, 'should be undefined at first');

  const testUser = {
    id: 1,
    username: 'test-user',
    email: 'test@email.com',
  };
  service.storeUser(testUser);

  const serviceUser = {
    id: service.id,
    username: service.username,
    email: service.email
  };

  assert.deepEqual(serviceUser, testUser, 'should set user in service');
});
