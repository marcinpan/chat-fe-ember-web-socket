import { moduleFor, test } from 'ember-qunit';

moduleFor('service:web-socket', 'Unit | Service | web socket', {
  needs: ['service:socketIo']
});

test('it exists', function(assert) {
  let service = this.subject();
  assert.ok(service);
  assert.ok(service.get('socket'), 'should define a socket');
  assert.equal(service.get('socket.socket.io.opts.port'), '3100', 'should set port to 3100');
});
