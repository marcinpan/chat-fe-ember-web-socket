import { moduleFor, test } from 'ember-qunit';

moduleFor('route:chat', 'Unit | Route | chat', {
  // Specify the other units that are required for this test.
  needs: ['service:session', 'service:currentUser', 'service:webSocket']
});

test('it exists', function(assert) {
  let route = this.subject();
  assert.ok(route);
});
