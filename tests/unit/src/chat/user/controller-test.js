import { moduleFor, test } from 'ember-qunit';

moduleFor('controller:chat/user', 'Unit | Controller | chat/user', {
  // Specify the other units that are required for this test.
  needs: ['service:session', 'service:currentUser', 'service:webSocket', 'service:socketIo']
});

// Replace this with your real tests.
test('it exists', function(assert) {
  let controller = this.subject();
  assert.ok(controller);
});
