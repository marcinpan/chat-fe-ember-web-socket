import { moduleFor, test } from 'ember-qunit';

moduleFor('route:signup', 'Unit | Route | signup', {
  // Specify the other units that are required for this test.
  needs: ['service:session', 'service:currentUser', 'service:webSocket', 'service:socketIo']
});

test('it exists', function(assert) {
  let route = this.subject();
  assert.ok(route);
});
